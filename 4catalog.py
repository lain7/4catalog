#!/usr/bin/env python3
"""
4Catalog
Copyright (C) 2018  lain7

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import json, urllib.request, sys
from datetime import datetime
from os import path
from imageboards import *

VERSION = "0.1.0"
def filterThreads(threads, filterFile):
  filteredThreads = []
  filterFile = open(filterFile, "r")
  try:
    threadFilter = json.loads(filterFile.read())
  except:
    print("Not a proper JSON file. Exiting...")
    filterFile.close()
    exit(0)
  filterFile.close()
  for i in range(len(threads)):
    bad=False
    for x in threadFilter:
      if (x in threads[i]["title"]):
        bad=True
      elif( x in threads[i]["main"]):
        bad=True
    if(bad):
      continue
    else:
      filteredThreads.append(threads[i])
  return filteredThreads

def sort(threads, sortMethod):
  sortedThread = []
  if(sortMethod == "bump"):
    sortMethod = "lastModified"
  while (threads):
    high = [0, 0] #number index
    for i in range(len(threads)):
      if (int(threads[i][sortMethod]) > int(high[0])):
        high[0] = threads[i][sortMethod]
        high[1] = i
    sortedThread.append(threads[high[1]])
    del threads[high[1]]
  return sortedThread

def generateHTML(threads, board, sortMethod, filterFile, boardColor, textColor):
  if (boardColor == "#1D1F21"):
    invertPercent = "100%"
  else:
    invertPercent = "0%"
  html = "<!DOCTYPE html><html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/></head><title>Lain's Catalog</title><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" media=\"screen\" /><body style=\"background:" + boardColor + ";color:" + textColor + ";\"><div id=\"banner\"><img src=\"icon.png\" style=\"filter: invert(" + invertPercent + ");\"/><br /><h2>Lain's Catalog</h2><h6>/" + board + "/ - Generated at " + str(datetime.now()) + " - Sorted by: " + sortMethod + " - Filter: " + filterFile + "</h6></div><div id=\"content\">"
  count = 0
  for i in range(len(threads)):
    html += "<div id=\"item\"><a href=\"" + threads[i]['link'] + "\"><div id=\"imgContainer\"><img src=\"" + threads[i]['img'] + "\" alt=\"404\"/></div></a>" + "<div class=\"text\">R: <strong>" + threads[i]["replies"] + "</strong> / I: <strong>" + threads[i]["images"] +"</strong></div><p>"
    if (threads[i]["title"] != ''):
      html += "<strong>" + threads[i]["title"] + "</strong>"
      if(threads[i]["main"] != ''):
        html += ": " + threads[i]["main"]
    else:
      html += threads[i]["main"]
    html += "</p></div>"
  html += "</div></body></html>"
  return html

def main():
  options = {
    "boards" : [],
    "sortMethod" : "bump",
    "filterFile" : "none",
    "boardColor" : "#EEF2FF",
    "textColor" : "black",
    }
  if (len(sys.argv) <= 1 or "-h" in sys.argv or "--help" in sys.argv):
    print("Usage: ./4catalog --board (board) --sort (sort type) --filter (filterFile)")
    print("\nArguments:")
    print("       -b or --board        Board for catalog view. Ex: -b wsg 8chan::tech")
    print("       -s or --sort         Sorts threads. Ex: --sort images or -s replies")
    print("       -f or --filter       Filters the board with a json file. (See example filter in GitLab repo) Ex: -f sortFile")
    print("       -bc or --boardColor  Changes the background color. Ex: --boardColor white or -bc #000000")
    print("       -tc or --textColor   Changes the text color. Ex: --textColor white or -tc #000000")
    print("       -t or --theme        Changes the background and text color with premade themes. Ex: --theme tm or -t yotsuba")
    print("       -h or --help         Display this help prompt")
    print("       -v or --version      Shows the version number")
    exit(0)
  for i in range(len(sys.argv)):
    try:
      if (sys.argv[i] == "-b" or sys.argv[i] == "--board"):
        eoa = False
        argCount = 0
        #non specified boards default to 4chan
        while(not eoa):
          if(not "::" in sys.argv[i+1+argCount]):
            options["boards"].append("4chan::" + sys.argv[i+1+argCount])
          else:
            options["boards"].append(sys.argv[i+1+argCount])
          if(len(sys.argv) <= i+2+argCount or "-" in sys.argv[i+2+argCount]):
            eoa = True
          argCount += 1
      elif(sys.argv[i] == "-s" or sys.argv[i] == "--sort"):
        options["sortMethod"] = sys.argv[i+1]
      elif(sys.argv[i] == "-f" or sys.argv[i] == "--filter"):
        options["filterFile"] = sys.argv[i+1]
      elif(sys.argv[i] == "-bc" or sys.argv[i] == "--boardColor"):
        options["boardColor"] = sys.argv[i+1]
      elif(sys.argv[i] == "-tc" or sys.argv[i] == "--textColor"):
        options["textColor"] = sys.argv[i+1]
      elif(sys.argv[i] == "-t" or sys.argv[i] == "--theme"):
        if (sys.argv[i+1] == "y" or sys.argv[i+1] == "yotsuba"):
          options["textColor"] = "#910000"
          options["boardColor"] = "#FFFFEE"
        elif (sys.argv[i+1] == "yb" or sys.argv[i+1] == "yotsubaB"):
          options["textColor"] = "black"
          options["boardColor"] = "#EEF2FF"
        elif (sys.argv[i+1] == "tm" or sys.argv[i+1] == "tomorrow"):
          options["textColor"] = "#BFC8C6"
          options["boardColor"] = "#1D1F21"
      elif(sys.argv[i] == "-v" or sys.argv[i] == "--version"):
          print("4catalog " + VERSION)
    except:
      print("The argument \"" + sys.argv[i] + "\" is missing a variable.")
      exit(0)
    catalogThreads = []
  for i in range(len(options["boards"])):
    tempBoard = options["boards"][i].split("::")
    try:
      if (tempBoard[0] == "4chan"):
        catalogThreads += get4Catalog(tempBoard[1])
      elif (tempBoard[0] == "8chan"):
        catalogThreads += get8Catalog(tempBoard[1])
      elif (tempBoard[0] == "arisu"):
        catalogThreads += getArisuCatalog(tempBoard[1])
      else:
        print("Missing board! Exiting...")
        exit(0)
    except urllib.error.HTTPError as e:
      print("Failed to retrieve\"" + options["boards"][i] + "\" due to an " + str(e))
  if(not options["sortMethod"] in ["images", "replies"]):
    options["sortMethod"] = "bump"
  if(options["sortMethod"] == "images" or
     options["sortMethod"] == "replies" or
     (len(options["boards"]) > 1 and options["sortMethod"] == "bump")):
    catalogThreads = sort(catalogThreads, options["sortMethod"])
  if(path.isfile(options["filterFile"]) and options["filterFile"] != "none"):
    catalogThreads = filterThreads(catalogThreads, options["filterFile"])
  elif(options["filterFile"] != "none"):
    print("A filter was specified, but the file does not exist. Continuing...")
    options["filterFile"] = "none"
  htmlFile = open("gen.html", 'w')
  htmlFile.write(generateHTML(
                   catalogThreads,
                   str(options["boards"]).replace("/", '').replace("[", '').replace("]", '').replace("\'", ''),
                   options["sortMethod"],
                   options["filterFile"],
                   options["boardColor"],
                   options["textColor"]))
  htmlFile.close()

if __name__ == "__main__":
  main()
