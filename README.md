# DEPRECATED - See [Navichan](https://gitlab.com/lain7/navichan)

4Catalog v0.1.0

[How it looks as of January 15, 2018](https://gitlab.com/lain7/4catalog/raw/master/2018-01-15.png)

A program to generate a catalog view for image boards like 4chan, 8chan, and arisuchan.
Meant as an alternative to running 4chan's non-free javascript to view the catalog.

Features:
- Combine as many boards as you want, even from different sites.
- Sort the catalog by images, replies, and bump order.
- Filter threads with keywords.
- Themes

Supported Boards:
- 4chan (The default for boards. For example you do not have to type 4chan::g, you can just type g)
- 8chan (8chan::tech)
- Arisuchan (arisu::cyb)

- Dependencies:
  - python3

Usage: './4catalog.py arguments...'

Available themes:
- y or yotsuba
- yb or yotsubaB
- tm or tomorrow

See the example filter file to get an idea how to use the filter.

|     Argument      |                  Function                  |   Example    |
|-------------------|--------------------------------------------|--------------|
|   -b or --board   | Specifies which board to generate a catalog|   -b wsg     |
|   -s or --sort    | Sorts by image or reply count              |  -s images   |
|   -f or --filter  | Uses a json file to filter out threads     | -f filterFile|
|-bc or --boardColor| Changes the background color               | -bc #000000  |
|-tc or --textColor | Changes the text color                     | -tc #ff0000  |
|   -t or --theme   | Changes the theme                          | -t tm        |
|  --help or -h   | Displays help text                           | -h           |
| --version or -v | Displays the version of the program          | -v           |
